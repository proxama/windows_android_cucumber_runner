# Cucumdroid Cucumber Runner #
This is a simple GUI front-end on the Cucumdroid cucumber runner.  It provides a relatively user-friendly was to run the tests and view the results.

## Usage ##

This library that you have both ANT and the Android SDK on your path.

Either browse to root of your project that contains the Cucumdroid annotated tests.

Clicking run tests will cause the application to execute an ANT script to compile the 

	- src
	- resource
folders from the directory, jar them, dex them and put them on the device. It will then execute the test and parse the results file to be displayed.


----------


### This application requires ###
.Net 4

ant

Android SDK

[Cucumdroid annotations](https://bitbucket.org/proxama/android_cucumdroid_annotations/overview "Cucumdroid")

[Cucumdroid](https://bitbucket.org/proxama/android_cucumdroid/overview "Cucumdroid")

----------
### License ###
This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB "details").
