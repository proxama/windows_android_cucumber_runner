﻿/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CucumberRunner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            FileSystemWatcher watcher = new FileSystemWatcher(System.Environment.CurrentDirectory, "results.json");
            watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.LastAccess | NotifyFilters.Size;
            watcher.Changed += watcher_Changed;
            watcher.EnableRaisingEvents = true;
            
            ImageList imageList = new ImageList();
            imageList.Images.Add(Image.FromFile("cross.png"));
            imageList.Images.Add(Image.FromFile("tick.png"));

            treeResults.ImageList = imageList;
        }

        private void btnRunTests_Click(object sender, EventArgs e)
        {
            tbConsoleOut.Text = String.Empty;
            treeResults.Nodes.Clear();

            createLocalProperties();

            // Start the child process.
            Process p = new Process();

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "cmd";
            p.StartInfo.CreateNoWindow = true;

            p.EnableRaisingEvents = true;
            p.OutputDataReceived += new DataReceivedEventHandler(ConsoleOutputHandler);

            p.Start();
            p.StandardInput.WriteLine("ant uitest");
            p.BeginOutputReadLine();

            btnRunTests.Enabled = false;
        }

        private void createLocalProperties()
        {
            string filename = "build.properties";
            string data = "test.src=" + textBox1.Text.Replace("\\", "/") + Environment.NewLine;
            data += "tests.upload.dir=" + "/sdcard/cucumdroid_example/" + Environment.NewLine;

            using (StreamWriter writer = new StreamWriter(filename))
            {
                writer.Write(data);
            }
        }

        private void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            tbConsoleOut.BeginInvoke((Action)(() =>
            {
                ReadTestResults();
                btnRunTests.Enabled = true;
            }));
        }

        private void ReadTestResults()
        {
            string jsonResults = String.Empty;
            using (StreamReader stream = new StreamReader("results.json"))
            {
                jsonResults = stream.ReadToEnd();
            }

            if (!String.IsNullOrEmpty(jsonResults))
            {
                ResultSet results = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultSet>(jsonResults);
                ProcessResults(results);
            }
        }

        private void ProcessResults(ResultSet results)
        {
            List<TreeNode> featureNodes = new List<TreeNode>();
            foreach (Feature feature in results.features)
            {
                List<TreeNode> scenarioNodes = new List<TreeNode>();
                foreach (Scenario scenario in feature.scenarios)
                {
                    List<TreeNode> stepNodes = new List<TreeNode>();
                    foreach (Step step in scenario.steps)
                    {
                        stepNodes.Add(new TreeNode(step.name, getImageIndex(step.success), getImageIndex(step.success)));
                    }
                    scenarioNodes.Add(createNode(scenario.name, stepNodes, scenario.successful));
                }
                featureNodes.Add(createNode(feature.name, scenarioNodes, feature.successful));
            }

            treeResults.Nodes.Clear();
            treeResults.Nodes.AddRange(featureNodes.ToArray<TreeNode>());
        }

        private TreeNode createNode(string name, List<TreeNode> children, bool successful)
        {                   
            TreeNode node = new TreeNode(name, getImageIndex(successful), getImageIndex(successful), children.ToArray<TreeNode>());
            if (!successful)
            {
                node.Expand();
            }

            return node;
        }

        private int getImageIndex(bool isSuccessful)
        {
            return isSuccessful ? 1 : 0;
        }

        private void ConsoleOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            tbConsoleOut.BeginInvoke((Action)(() => {
            tbConsoleOut.Text += outLine.Data + Environment.NewLine;
            tbConsoleOut.SelectionStart = tbConsoleOut.Text.Length;
            tbConsoleOut.ScrollToCaret();
            }));
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
