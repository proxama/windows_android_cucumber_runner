﻿/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CucumberRunner
{
    class Feature
    {
        public string name { get; set; }
        public int scenariosCount { get; set; }
        public bool successful { get; set; }

        public Scenario[] scenarios { get; set; }
    }
}
